
//
//  WebView_ViewController.swift
//  EMI calculator
//
//  Created by iOS-Appentus on 07/February/2020.
//  Copyright © 2020 appentus. All rights reserved.
//

import UIKit

class WebView_ViewController: UIViewController {
    @IBOutlet weak var webview:UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        webview.loadRequest(URLRequest (url: URL (string:"https://m.11086999.com/")!))
    }
    
    @IBAction func btn_cancel(_ sender:Any) {
        dismiss(animated: true, completion: nil)
    }

}
