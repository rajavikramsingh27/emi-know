//  Common_ViewController.swift
//  Common
//  Created by iOS-Appentus on 01/February/2020.
//  Copyright © 2020 appentus. All rights reserved.


import UIKit
import StoreKit



class Common_ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UIPickerViewDelegate,UIPickerViewDataSource {
    
    let arr_common = ["Language","Contact us","Apple Review","Share a friend","Report"]
    
    let arr_language = ["English",
                        "Hindi",
                        "Japanese"]
    
    var picker_view = UIPickerView()
    var toolBar = UIToolbar()
    
    var index_language = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.setNavigationBarHidden(true, animated:true)
        
        picker_view = UIPickerView (frame: CGRect (x: 0, y:self.view.bounds.height-240, width: self.view.bounds.width, height: 240))
        self.view.addSubview(picker_view)
        
        toolBar = UIToolbar (frame: CGRect (x:0, y:self.view.bounds.height-280, width: self.view.bounds.width, height: 40))
        toolBar.barStyle = UIBarStyle.default
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(onDoneBarButtonClick(_:)))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        toolBar.setItems([spaceButton, doneButton], animated: false)
        self.view.addSubview(toolBar)
        self.view.bringSubviewToFront(toolBar)
        
        picker_view.backgroundColor = UIColor .lightGray//.withAlphaComponent(0.4)
        picker_view.isHidden = true
        toolBar.isHidden = true
        picker_view.delegate = self
        picker_view.dataSource = self
    }
    
    @IBAction func onDoneBarButtonClick (_ sender:UIButton) {
        picker_view.isHidden = true
        toolBar.isHidden = true
            
        if index_language == 0 {
            languageButtonAction(Language.english.rawValue)
        } else if index_language == 1 {
            languageButtonAction(Language.hindi.rawValue)
        } else if index_language == 2 {
            languageButtonAction(Language.japanese.rawValue)
        }
        
        UserDefaults.standard.set("\(index_language)", forKey: "lang")
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arr_common.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        cell.textLabel?.text = arr_common[indexPath.row].localized()
        cell.textLabel?.textAlignment = .center
        cell.textLabel?.font = UIFont (name:"Times New Roman", size: 16.0)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            picker_view.isHidden = false
            toolBar.isHidden = false
        } else if indexPath.row == 1 {
            let contact = self.storyboard?.instantiateViewController(withIdentifier:"Contact_Us_ViewController") as! Contact_Us_ViewController
            self.navigationController?.pushViewController(contact, animated:true)
        } else if indexPath.row == 2 {
            rateApp()
        } else if indexPath.row == 3 {
           shareTextButton()
        } else if indexPath.row == 3 {
           let contact = self.storyboard?.instantiateViewController(withIdentifier:"Report_ViewController") as! Report_ViewController
           self.navigationController?.pushViewController(contact, animated:true)
       }
        
    }
        
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arr_language.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arr_language[row].localized()
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        index_language = row
    }
    
    func shareTextButton() {
        let text = "We will share appstore link after live.".localized()
        let textShare = [text]
        let activityViewController = UIActivityViewController(activityItems: textShare , applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    func rateApp() {
        func_alert("We will use store kit after app live.".localized())
        
//        if #available(iOS 10.3, *) {
//            SKStoreReviewController.requestReview()
//        } else {
//            let appID = "Your App ID on App Store"
//            let urlStr = "https://itunes.apple.com/app/id\(appID)" // (Option 1) Open App Page
////            let urlStr = "https://itunes.apple.com/app/id\(appID)?action=write-review" // (Option 2) Open App Review Page
//
//            guard let url = URL(string: urlStr), UIApplication.shared.canOpenURL(url) else { return }
//
//            if #available(iOS 10.0, *) {
//                UIApplication.shared.open(url, options: [:], completionHandler: nil)
//            } else {
//                UIApplication.shared.openURL(url) // openURL(_:) is deprecated from iOS 10.
//            }
//        }
    }
    
}


extension UIViewController {
    @IBAction func btn_back(_ sender:Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

