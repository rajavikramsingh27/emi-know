//  Language.swift
//  EMI calculator
//  Created by iOS-Appentus on 05/February/2020.
//  Copyright © 2020 appentus. All rights reserved.


import Foundation
import UIKit

var bundleKey: UInt8 = 0

class AnyLanguageBundle: Bundle {
override func localizedString(forKey key: String,value: String?,table tableName: String?) -> String {
    guard
        let path = objc_getAssociatedObject(self, &bundleKey) as? String,
        let bundle = Bundle(path: path) else {
            return super.localizedString(forKey: key, value: value, table: tableName)
    }
    return bundle.localizedString(forKey: key, value: value, table: tableName)
  }
}

extension Bundle {
    class func setLanguage(_ language: String) {
        defer {
            object_setClass(Bundle.main, AnyLanguageBundle.self)
        }
        objc_setAssociatedObject(Bundle.main, &bundleKey,Bundle.main.path(forResource: language, ofType: "lproj"), .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
      }
}

extension String {
    func localized() ->String {
        var str_lang = ""
        if let lang = UserDefaults.standard.object(forKey:"lang") as? String {
            if lang == "0" {
                str_lang = Language.english.rawValue
            } else if lang == "1" {
                str_lang = Language.hindi.rawValue
            } else if lang == "2" {
                str_lang = Language.japanese.rawValue
            }
        }
        
        let path = Bundle.main.path(forResource:str_lang, ofType: "lproj")
        let bundle = Bundle(path: path!)

        return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: "")
    }
    
}

    
func languageButtonAction(_ lang:String) {
//        UserDefaults.standard.set([lang], forKey: "AppleLanguages")
//        UserDefaults.standard.synchronize()

    Bundle.setLanguage(lang)
    let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
    UIApplication.shared.keyWindow?.rootViewController = storyboard.instantiateInitialViewController()
}
    


enum Language: String {
    case english = "en"
    case hindi = "hi"
    case japanese = "ja"
}


