//  AppDelegate.swift
//  EMI calculator
//  Created by iOS-Appentus on 27/04/19.
//  Copyright © 2019 appentus. All rights reserved.



import UIKit
import IQKeyboardManagerSwift
import UserNotifications

var is_disconnected = true

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        if #available(iOS 13, *) {
            window!.overrideUserInterfaceStyle = .light
        }
        
        IQKeyboardManager.shared.enable = true
        
        if let lang = UserDefaults.standard.object(forKey: "lang") as? String {
            if lang == "0" {
                languageButtonAction(Language.english.rawValue)
            } else if lang == "1" {
                languageButtonAction(Language.hindi.rawValue)
            } else if lang == "2" {
                languageButtonAction(Language.japanese.rawValue)
            }
        } else {
            languageButtonAction(Language.english.rawValue)
        }
        
        let center = UNUserNotificationCenter.current()
        center.delegate = self
        center.requestAuthorization(options: [.alert, .sound,.badge]) { (granted, error) in
            // Enable or disable features based on authorization.
            if error != nil {
                print("Request authorization failed!")
            } else {
                print("Request authorization succeeded!")
//                self.showAlert()
            }
        }
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
        
        is_disconnected = Reachability.isConnectedToNetwork()
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        DispatchQueue.main.asyncAfter(deadline: .now()+3) {
            if !is_disconnected {
    //            func_alert("2")
                is_disconnected = Reachability.isConnectedToNetwork()
                if is_disconnected {
//                    self.func_alert("3")
                    scheduleLocal()
                }
                
            }
        }
        
    }
    
    
    
    func func_alert(_ string:String) {
        let alert = UIAlertController (title: string, message: "", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "ok", style: .cancel, handler: nil))
        self.window?.rootViewController?.present(alert, animated: true, completion: nil)
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert,.badge,.sound])
        
        UNUserNotificationCenter.current().removeAllDeliveredNotifications()
        
        let webview = UIStoryboard (name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WebView_ViewController") as! WebView_ViewController
        self.window?.rootViewController?.present(webview, animated: true, completion: nil)
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        
    }
    
}

