//  EMI_Calculator_ViewController.swift
//  EMI calculator
//  Created by iOS-Appentus on 27/04/19.
//  Copyright © 2019 appentus. All rights reserved.



import UIKit



class EMI_Calculator_ViewController: UIViewController {
    @IBOutlet weak var view_fourth:UIView!
    
    @IBOutlet weak var txt_loan_amt:UITextField!
    @IBOutlet weak var lbl_enter_time:UILabel!
    @IBOutlet weak var lbl_total_interest:UILabel!
    @IBOutlet weak var lbl_emi_installment:UILabel!

    @IBOutlet weak var view_loan_amt:UIView!
    @IBOutlet weak var view_interest_rate:UIView!
    @IBOutlet weak var view_enter_time:UIView!
    @IBOutlet weak var view_Total:UIView!
    @IBOutlet weak var view_emi_installment:UIView!
    
    @IBOutlet weak var btn_calculate:UIButton!
    @IBOutlet weak var btn_reset:UIButton!
    
    
    @IBOutlet weak var picker_view:UIPickerView!
    @IBOutlet weak var view_date_picker:UIView!
    
    let arr_yrs = ["1","2","3","4","5"]
    var arr_months = [String]()
    
    var is_years = false
    
    var str_years = ""
    var str_months = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
                
        for i in 1..<32 {
            arr_months.append("\(i)")
        }
        
        print(arr_yrs.count)
        print(arr_months.count)
        
        view_date_picker.isHidden = true
        
        btn_calculate.layer.cornerRadius = 4
        btn_reset.layer.cornerRadius = 4
        
        func_border(view: view_fourth, width: 15)
//        func_border(view: view_fourth, width: 1)
        
        func_shadow(view: view_loan_amt, radius: 4)
        func_shadow(view: view_interest_rate, radius: 4)
        func_shadow(view: view_enter_time, radius: 4)
        func_shadow(view: view_Total, radius: 4)
        
        func_first()
        createGradientLayer()
    }
    

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
//    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//        self.view.endEditing(true)
//    }
    
    func func_first() {
        let color_first = UIColor (red: 236.0/255.0, green: 149.0/255.0, blue: 67.0/255.0, alpha: 1.0)
        let color_second = UIColor (red: 221.0/255.0, green: 38.0/255.0, blue: 98.0/255.0, alpha: 1.0)
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = view_emi_installment.bounds
        gradientLayer.colors = [color_first.cgColor, color_second.cgColor]
//        view_emi_installment.layer.addSublayer(gradientLayer)
        view_emi_installment.layer.insertSublayer(gradientLayer, at: 0)
        
//        self.view.bringSubviewToFront(view_emi_installment)
//        self.view.sendSubviewToBack(view_emi_installment)
    }
    
    func createGradientLayer() {
        let color_first = UIColor (red: 236.0/255.0, green: 149.0/255.0, blue: 67.0/255.0, alpha: 1.0)
        let color_second = UIColor (red: 221.0/255.0, green: 38.0/255.0, blue: 98.0/255.0, alpha: 1.0)
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = view_Total.bounds
        gradientLayer.colors = [color_first.cgColor, color_second.cgColor]
//        view_Total.layer.addSublayer(gradientLayer)
        
        view_Total.layer.insertSublayer(gradientLayer, at: 0)
//        self.view.bringSubviewToFront(view_Total)
    }
    
    func func_border(view:UIView,width:CGFloat) {
        let color_first = UIColor (red: 236.0/255.0, green: 149.0/255.0, blue: 67.0/255.0, alpha: 1.0)
        view.layer.cornerRadius = view.frame.size.height/2
        view.layer.borderColor = color_first .cgColor
        view.layer.borderWidth = width
        view.clipsToBounds = true
    }
    
    func func_shadow(view:UIView,radius:CGFloat) {
        view.layer.cornerRadius = radius
        view.layer.shadowOpacity = 1.5
        view.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        view.layer.shadowRadius = 1.5
        view.layer.shadowColor = UIColor .lightGray.cgColor
    }
    
    @IBAction func btn_show_date_picker(_ sender:UIButton)  {
        view_date_picker.isHidden = false
    }
    
    @IBAction func btn_done_datePIcker(_ sender:UIButton)  {
        view_date_picker.isHidden = true
    }
    
    @IBAction func btn_years(_ sender:UIButton)  {
        is_years = true
        picker_view.reloadAllComponents()
    }
    
    @IBAction func btn_months(_ sender:UIButton)  {
        is_years = false
        picker_view.reloadAllComponents()
    }
    
    @IBAction func btn_calulate(_ sender:UIButton) {
        scheduleLocal()
        return
        
        if txt_loan_amt.text!.isEmpty || txt_loan_amt.text == "0" {
            let alert = UIAlertController (title: "", message: "Enter loan amount".localized(), preferredStyle: .alert)
            let yes = UIAlertAction(title: "Ok".localized(), style: .default) { (yes) in
                
            }
            
            alert.addAction(yes)
            
            alert.view.tintColor = UIColor .black
            present(alert, animated: true, completion: nil)
            
            return
        } else if lbl_enter_time.text == "Enter time period".localized() {
            let alert = UIAlertController (title: "", message: "Enter time period".localized(), preferredStyle: .alert)
            let yes = UIAlertAction(title: "Ok", style: .default) { (yes) in
                
            }
            
            alert.addAction(yes)
            
            alert.view.tintColor = UIColor .black
            present(alert, animated: true, completion: nil)
            
            return
        }
        
        func_loan_calc()
    }
    
    func func_loan_calc()  {
        let double = Double(txt_loan_amt.text!)
        
        let calc_value = (6.0*double!)/100.0
        lbl_total_interest.text = String(format: "%.2f", calc_value+double!)
        lbl_emi_installment.text = "\(Int64(calc_value+double!)/6)"
        
        var arr_cal_amount = [[String:String]]()
        let dict_cal_amount = ["amt":txt_loan_amt.text!,"time":lbl_enter_time.text,"total":lbl_total_interest.text,"emi":lbl_emi_installment.text] as! [String : String]
        if var arr_calculation = UserDefaults.standard.object(forKey:"calculation") as? [[String:String]] {
            arr_cal_amount = arr_calculation
            
            arr_calculation.removeLast()
            
            arr_cal_amount.append(dict_cal_amount)
        } else {
            arr_cal_amount.append(dict_cal_amount)
        }
        UserDefaults.standard.setValue(arr_cal_amount, forKey:"calculation")
    }
    
    @IBAction func btn_reset(_ sender:UIButton) {
        txt_loan_amt.text = ""
        lbl_enter_time.text = "Enter time period".localized()
        lbl_total_interest.text = "0"
        lbl_emi_installment.text = "0"
    }
    
    

}



extension EMI_Calculator_ViewController : UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if is_years {
            return arr_yrs.count
        } else {
            return arr_months.count
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if is_years {
            return arr_yrs[row]
        } else {
            return arr_months[row]
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if is_years {
            str_years = "\(arr_yrs[row]) yrs"
        } else {
            str_months = "\(arr_months[row]) months"
        }
        
        if str_years.isEmpty {
            str_years = "0"
        }
        
        if str_months.isEmpty {
            str_months = "0"
        }
        
        lbl_enter_time.text = "\(str_years) / \(str_months)"
    }
    
}



 func scheduleLocal() {
    let content = UNMutableNotificationContent()
    content.title = NSString.localizedUserNotificationString(forKey:"Internet Connection is Available", arguments: nil)
    content.body = NSString.localizedUserNotificationString(forKey:"", arguments: nil)
    content.sound = UNNotificationSound.default
    content.categoryIdentifier = "notify-test"
    
    let trigger = UNTimeIntervalNotificationTrigger.init(timeInterval:0.2, repeats: false)
    let request = UNNotificationRequest.init(identifier: "notify-test", content: content, trigger: trigger)
    
    let center = UNUserNotificationCenter.current()
    center.add(request)
}
    

