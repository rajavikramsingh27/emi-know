//  Records_ViewController.swift
//  EMI calculator
//  Created by iOS-Appentus on 01/February/2020.
//  Copyright © 2020 appentus. All rights reserved.


import UIKit


class Records_ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var lbl_records:UILabel!
    @IBOutlet weak var tbl_records:UITableView!
    var arr_cal_amount = [[String:String]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        func_set_UI()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 107
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arr_cal_amount.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! Records_TableViewCell
        
        cell.lbl_loan_amt.text = arr_cal_amount[indexPath.row]["amt"]
        cell.lbl_time.text = arr_cal_amount[indexPath.row]["time"]
        cell.lbl_total.text = arr_cal_amount[indexPath.row]["total"]
        cell.lbl_emi.text = arr_cal_amount[indexPath.row]["emi"]
                
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            arr_cal_amount.remove(at: indexPath.row)
            UserDefaults.standard.setValue(arr_cal_amount, forKey:"calculation")
            tbl_records.reloadData()
            
            func_set_UI()
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let animation = AnimationFactory.makeSlideIn(duration: 0.5, delayFactor: 0.05)
        let animator = Animator(animation: animation)
        animator.animate(cell: cell, at: indexPath, in: tableView)
    }
        
    func func_set_UI() {
        if let arr_calculation = UserDefaults.standard.object(forKey:"calculation") as? [[String:String]] {
            tbl_records.isHidden = false
            
            if arr_calculation.count == 0 {
                tbl_records.isHidden = true
            } else {
                arr_cal_amount = arr_calculation
                tbl_records.reloadData()
            }
        } else {
            tbl_records.isHidden = true
        }
    }
    
}


