//
//  Report_ViewController.swift
//  Common
//
//  Created by iOS-Appentus on 01/February/2020.
//  Copyright © 2020 appentus. All rights reserved.


import UIKit
import MBProgressHUD

class Report_ViewController: UIViewController {
    @IBOutlet weak var txt_comments:UITextView!
    @IBOutlet weak var btn_submit:UIButton!
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txt_comments.textColor = UIColor .lightGray
        txt_comments.func_set_border()
        btn_submit.func_set_border()
        
        txt_comments.text = "Enter your report...".localized()
    }
    
    @IBAction func btn_submit(_ sender:Any) {
        if txt_comments.text!.isEmpty || txt_comments.text! == "Enter your report...".localized() {
            func_alert("Enter your report".localized())
            return
        }
        
        MBProgressHUD.showAdded(to:self.view, animated: true)
        DispatchQueue.main.asyncAfter(deadline: .now()+2) {
            MBProgressHUD.hide(for:self.view, animated: true)
            self.func_alert("Your report submit successfully".localized())
            DispatchQueue.main.asyncAfter(deadline: .now()+2) {
                self.navigationController?.popViewController(animated: true)
            }
        }
        
    }
}


extension Report_ViewController:UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "Enter your report...".localized() {
            textView.textColor = UIColor .blue
            textView.text = ""
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            textView.textColor = UIColor .lightGray
            textView.text = "Enter your report...".localized()
        }
    }
    
    
}
