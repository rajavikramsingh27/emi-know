//  Contact_Us_ViewController.swift
//  Common
//  Created by iOS-Appentus on 01/February/2020.
//  Copyright © 2020 appentus. All rights reserved.


import UIKit
import MBProgressHUD


class Contact_Us_ViewController: UIViewController {
    @IBOutlet weak var txt_email:UITextField!
    @IBOutlet weak var txt_name:UITextField!
    @IBOutlet weak var txt_comments:UITextView!
    @IBOutlet weak var btn_submit:UIButton!
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txt_comments.textColor = UIColor .lightGray
        txt_comments.func_set_border()
        btn_submit.func_set_border()
        
        txt_comments.text = "Enter your comments... (Optional!)".localized()
    }
    
    @IBAction func btn_submit(_ sender:Any) {
        if txt_email.text!.isEmpty {
            func_alert("Enter your email".localized())
            return
        } else if !txt_email.text!.isValidEmail() {
            func_alert("Enter a valid email".localized())
            return
        } else if txt_name.text!.isEmpty {
            func_alert("Enter your name".localized())
            return
        }
        
        MBProgressHUD.showAdded(to:self.view, animated: true)
        DispatchQueue.main.asyncAfter(deadline: .now()+2) {
            MBProgressHUD.hide(for:self.view, animated: true)
            self.func_alert("Our team will contact you soon".localized())
            
            DispatchQueue.main.asyncAfter(deadline: .now()+2) {
                self.navigationController?.popViewController(animated: true)
            }
        }
        
    }
}



extension UIView {
    func func_set_border() {
        self.layer.cornerRadius = 10
        self.layer.borderColor = UIColor .lightGray .cgColor
        self.layer.borderWidth = 1
        self.clipsToBounds = true
    }
}



extension Contact_Us_ViewController:UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "Enter your comments... (Optional!)".localized() {
            textView.textColor = UIColor .blue
            textView.text = ""
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            textView.textColor = UIColor .lightGray
            textView.text = "Enter your comments... (Optional!)".localized()
        }
    }
    
    
}



extension UIViewController {
    func func_alert(_ msg:String) {
        let alert_VC = UIAlertController (title:msg, message:"" , preferredStyle: .alert)
        present(alert_VC, animated: true, completion: nil)
        
        DispatchQueue.main.asyncAfter(deadline:.now()+1) {
            alert_VC.dismiss(animated: true, completion: nil)
        }
    }

}

extension String {
    func isValidEmail() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"

        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with:self)
    }

}
