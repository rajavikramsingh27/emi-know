//  ViewController.swift
//  EMI calculator
//  Created by iOS-Appentus on 27/04/19.
//  Copyright © 2019 appentus. All rights reserved.


import UIKit
import AngleGradientBorderView
import UserNotifications


class ViewController: UIViewController {
    @IBOutlet weak var view_first:UIView!
    @IBOutlet weak var view_second:UIView!
    @IBOutlet weak var view_third:UIView!
    @IBOutlet weak var view_fourth:AngleGradientBorderView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        func_first(view: view_first)
        func_shadow(view: view_second, radius: 6)
        func_shadow(view: view_third, radius:30)
        func_border(view: view_fourth)
        
        DispatchQueue.main.asyncAfter(deadline:.now()+3) {
            let emi_calculator = self.storyboard?.instantiateViewController(withIdentifier: "Tabbar") as! Tabbar
            self.navigationController?.pushViewController(emi_calculator, animated: true)
        }
        
        
    }
    
 
    func func_first(view:UIView) {
        let gradientColor = CAGradientLayer()
        gradientColor.frame = view.frame
        
        let color_first = UIColor (red: 236.0/255.0, green: 149.0/255.0, blue: 67.0/255.0, alpha: 1.0)
        let color_second = UIColor (red: 221.0/255.0, green: 38.0/255.0, blue: 98.0/255.0, alpha: 1.0)
        
        gradientColor.colors = [color_first.cgColor,color_second.cgColor]
        view.layer.insertSublayer(gradientColor, at: 0)
    }
    
    func func_shadow(view:UIView,radius:CGFloat)  {
        view.layer.cornerRadius = radius
        view.layer.shadowOpacity = 0.2
        view.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        view.layer.shadowRadius = 1.5
        view.layer.shadowColor = UIColor .lightGray.cgColor
    }
    
    func func_border(view:UIView) {
        let color_first = UIColor (red: 236.0/255.0, green: 149.0/255.0, blue: 67.0/255.0, alpha: 1.0)
        view.layer.cornerRadius = view.frame.size.height/2
        view.layer.borderColor = color_first .cgColor
        view.layer.borderWidth = 20
        view.clipsToBounds = true
    }
    
    
    
}




//  MARK:- UICollectionView methods
extension EMI_Calculator_ViewController:UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize (width: 30, height: collectionView.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 100
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let coll_Car = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
        
        return coll_Car
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
}

