//
//  Records_TableViewCell.swift
//  EMI calculator
//
//  Created by iOS-Appentus on 01/February/2020.
//  Copyright © 2020 appentus. All rights reserved.
//

import UIKit

class Records_TableViewCell: UITableViewCell {
    @IBOutlet weak var lbl_loan_amt:UILabel!
    @IBOutlet weak var lbl_time:UILabel!
    @IBOutlet weak var lbl_total:UILabel!
    @IBOutlet weak var lbl_emi:UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
